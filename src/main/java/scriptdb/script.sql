
CREATE TABLE email(
   id SERIAL PRIMARY KEY,
   remetente VARCHAR(80) NOT NULL,
   destinatarios VARCHAR NOT NULL,
   assunto VARCHAR(80),
   status BOOLEAN DEFAULT FALSE,
   mensagem TEXT,
   ipServidor VARCHAR(80) NOT NULL
);
