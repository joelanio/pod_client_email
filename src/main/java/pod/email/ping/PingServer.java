/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pod.email.ping;

import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Joew
 */
public class PingServer {
    public static boolean ping(String server) {
        
        boolean result = false;
        try {
            Socket socket = new Socket(server, 9999);
            result = true;
        } catch (IOException ex){
            System.out.println("Oops, servidor indisponivel...");
        }
        
        return result;
    }
}
