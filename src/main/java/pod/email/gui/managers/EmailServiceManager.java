/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pod.email.gui.managers;

import br.edu.ifpb.emailsharedpod.Email;
import br.edu.ifpb.emailsharedpod.Fachada;
import br.edu.ifpb.emailsharedpod.Pessoa;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import pod.email.dao.EmailDAO;

/**
 *
 * @author Joew
 */
public class EmailServiceManager {
    private String ip;
    private String nome;

    public EmailServiceManager(String ip, String nome) {
        this.ip = ip;
        this.nome = nome;
    }

    public EmailServiceManager() {
    }
    
    
    
    
    public List<String> getEmailsList() throws RemoteException, NotBoundException{
        
        //acessando a lista de pessoas do servidor
        try{
            //conexão
            Registry r = LocateRegistry.getRegistry(ip, 9999);
            System.out.println("conexão estabelecida com "+ip);
            Fachada h = (Fachada) r.lookup("Fachada");
            
            //acessando lista de pessoas remota
            List<Pessoa> pessoas = h.listaPessoas();
            
            //transferindo emails para uma lista de string
            List<String> emails = new ArrayList<>();
            
            for(Pessoa p: pessoas){
                emails.add(p.getEmail());
                
            }
            
           return emails;
            
        } catch (ConnectException ce){//tratando erro de conexão
            JOptionPane.showMessageDialog(null, "Falha na conexão com o servidor");
            return null;
        }
        
        
                                    
    }
    
    
    public String getEmailListAsString(List<String> emails){
        String st = new String();
        
        //concatenando a string com cada email
        for(String s: emails){
            st = st + s + ", ";
        }
        
        //eliminando a última virgula e o último espaço
        String stEmails = st.substring(0, st.length()-2);
        return stEmails;
        
    }
    
    
    public boolean cadastraPessoa(Pessoa pessoa){
         try{
            Registry r = LocateRegistry.getRegistry(this.ip, 9999);
            Fachada h = (Fachada) r.lookup("Fachada");
            h.salvar(pessoa);
            JOptionPane.showMessageDialog(null, "Pessoa cadastrada com sucesso");
            return true;
            
        } catch (Exception e){
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar usuario");
            return false;
        }
    }    
    
    
    
    public boolean salvaEmail(Email email){
        //salvando o email no banco
        EmailDAO emDao = new EmailDAO();
        return emDao.salvar(email);
    }
    
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
   
}
