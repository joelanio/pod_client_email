/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pod.email.dao;

import java.util.List;



/**
 *
 * @author Joew
 */
public interface DAO<T> {
    public boolean salvar(T t);
    public boolean remover(T t);
    public List<T> listar();
    public boolean atualizar(T pojo);
    public T buscar(Object id);
    
}
