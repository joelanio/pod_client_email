/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pod.email.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Joew
 */
public class ConnectionDAO {

    private final String url = "jdbc:postgresql://localhost:5432/client_email_pod";
    private final String user = "postgres";
    private final String password = "12345";
    protected Connection conn;

    public Connection getConnection() {
        try {
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
        }
        return conn;
    }

    public void fechar() {
        this.fehar(this.conn);
    }

    private void fehar(Connection connec) {
        try {
            connec.close();
        } catch (SQLException e) {
        }
    }
}
