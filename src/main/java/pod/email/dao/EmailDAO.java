/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pod.email.dao;

import br.edu.ifpb.emailsharedpod.Email;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Joew
 */
public class EmailDAO implements DAO<Email>{
    @Override
    public boolean salvar(Email pojo) {
        try {
            Connection conn = new ConnectionDAO().getConnection();
            String sql = "insert into email (remetente,destinatarios,assunto,mensagem,ipServidor) values (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            // setando os valores
            stmt.setString(1, pojo.getRemetente());
            stmt.setString(2, pojo.getDestinatarios());
            stmt.setString(3, pojo.getAssunto());
            stmt.setString(4, pojo.getMensagem());
            stmt.setString(5, pojo.getIpServidor());

            // executando
            stmt.execute();
            System.out.println("Emial adicionada à base de dados!");

            //finalisar connecções
            stmt.close();
            conn.close();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(EmailDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean remover(Email pojo) {
        try {
            String sql = "delete from email where id = " + pojo.getId();
            Connection conn = new ConnectionDAO().getConnection();
            Statement stmt = conn.createStatement();
            // executando
            stmt.executeQuery(sql);
            System.out.println("Email removido da base de dados!");
            //finalisar connecções
            stmt.close();
            conn.close();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(EmailDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    public List<Email> listar() {
        List<Email> lista = new ArrayList<>();
        try {
            String sql = "select * from email where status = false";
            Connection conn = new ConnectionDAO().getConnection();
            Statement stmt = conn.createStatement();
            // executando
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                Email email;
                email = new Email();
                email.setId(rs.getInt("id"));
                email.setAssunto(rs.getString("assunto"));
                email.setMensagem(rs.getString("mensagem"));
                email.setRemetente(rs.getString("remetente"));
                email.setDestinatarios(rs.getString("destinatarios"));
                email.setIpServidor(rs.getString("ipServidor"));
                lista.add(email);
            }
            
           
            //finalisar connecções
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(EmailDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    
    @Override
    public boolean atualizar(Email pojo) {
        try {
            Connection conn = new ConnectionDAO().getConnection();
            String sql = "update email set remetente =? ,destinatarios=?,assunto =?,status=?, mensagem= ?,ipServidor=? where id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            // setando os valores
            stmt.setString(1, pojo.getRemetente());
            stmt.setString(2, pojo.getDestinatarios());
            stmt.setString(3, pojo.getAssunto());
            stmt.setBoolean(4, pojo.isStatus());  
            stmt.setString(5, pojo.getMensagem());
            stmt.setString(6, pojo.getIpServidor());
            stmt.setInt(7, pojo.getId());

            // executando
            stmt.execute();
            System.out.println("Emial atualizado na base de dados!");

            //finalisar connecções
            stmt.close();
            conn.close();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(EmailDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public Email buscar(Object id) {
        Email email = new Email();
        try {
            String sql = "select * from email where id = ?";
            Connection conn = new ConnectionDAO().getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, (int) id);
            
            // executando
            ResultSet rs = stmt.executeQuery();
            
            if(rs.next()){
                email.setId(rs.getInt("id"));
                email.setAssunto(rs.getString("assunto"));
                email.setMensagem(rs.getString("mensagem"));
                email.setRemetente(rs.getString("remetente"));
                email.setDestinatarios(rs.getString("destinatarios"));
                email.setIpServidor(rs.getString("ipServidor"));
                
            }
            
            
            //finalisar connecções
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(EmailDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return email;
    }


}
