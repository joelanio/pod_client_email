/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pod.email.task;

import br.edu.ifpb.emailsharedpod.Email;
import br.edu.ifpb.emailsharedpod.Fachada;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import pod.email.dao.EmailDAO;
import pod.email.ping.PingServer;

/**
 *
 * @author Joew
 */
public class Agendador extends TimerTask{

    @Override
    public void run() {
        
        EmailDAO emDao = new EmailDAO();
    
        //obetndo a lista do banco
        List<Email> emailsDoBanco = new ArrayList<>();
        emailsDoBanco   = emDao.listar();
        
        System.out.println("Há "+emailsDoBanco.size()+" emails pendentes");
        //verificando se há emails, se houver tentar fazer lookup em cada um
        if(emailsDoBanco.size()>0){
        
        
            System.out.println("tentando enviar emails pendentes");
            //percorrendo a lista para v se tem email direcionado ao servidor atual
            for(Email e : emailsDoBanco){
                
                //tentativa de ping no servidor
                if(PingServer.ping(e.getIpServidor())){
                        //salvando o tempo inicial para cálculo de latencia
                        Long t0 = System.currentTimeMillis();
 
                        //tentando fazer lookup                       
                        try{
                        //verificando lookup
                        Registry r = LocateRegistry.getRegistry(e.getIpServidor(), 9999);
                        Fachada f = (Fachada) r.lookup("Fachada");
                        
                        
                        //calculando latencia
                        byte[] request = new byte[1024];

                        request[0] = '0';
                        request[1] = '1';
                        request[2] = '2';
                        request[3] = '3';
                        
                        long tOp = f.latencia(request).longValue();

                        long tf = System.currentTimeMillis();

                        long tt = tf - t0;

                        float latency = tt - tOp;   
                       
                        //calculando largura de banda
                        float bandwidth = (1024 / latency / 2) * 1000;
                        String stBandwidth = String.valueOf(bandwidth);
                        
                        
                        //salvando a largura da banda no corpo da mensagem do email
                        String novaMensagem = e.getMensagem()+"\nLargura de Banda: "+bandwidth;
                        e.setMensagem(novaMensagem);
                        
                        //enviando email, se retornar alguma string atualizar status
                        if(f.enviaEmail(e)!=null){
                            Email em = emDao.buscar(e.getId());
                            em.setStatus(true);
                            em.setMensagem(e.getMensagem());
                            System.out.println(em.getIpServidor());
                            emDao.atualizar(em);
                            System.out.println("Email enviado com sucesso!");
                        }else System.out.println("Erro ao enviar email!");

                        }catch(ConnectException exp){
                            System.out.println("erro ao tentar se conectar com rost "+ e.getIpServidor());
                        } catch (RemoteException | NotBoundException  ex) {
                            Logger.getLogger(Agendador.class.getName()).log(Level.SEVERE, null, ex);
                        } catch(Exception exp2){
                            exp2.printStackTrace();
                        }
                }
            }    
            
        }
        
    }
    
    
}
