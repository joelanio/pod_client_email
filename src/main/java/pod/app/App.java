package pod.app;

import java.util.Timer;
import javax.swing.JFrame;
import javax.swing.UIManager;
import pod.email.gui.HomeEmailShared;
import pod.email.task.Agendador;

/**
 *
 * @author marciel
 */
public class App {

    public static void main(String[] args) {
        Timer timer = new Timer();
        
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {}
        
        JFrame janela = new HomeEmailShared();
        janela.setResizable(false);
        janela.setVisible(true);
        timer.schedule(new Agendador(), 0, 300 * 60);
    }
}
